=======================
Beez Window Detailing
-----------------------
http://beezhd.com/


### Collaborative Tools
* [Github](https://github.com/Outc4sted/beez) - Source Control / Issues

### Server Dependencies:

* [Node.js](http://nodejs.org/)
* [Node Package Manager (npm)](http://npmjs.org/)

Once you have these installed, run `npm install` to install node libraries listed in `package.json`.
If you add a server-side dependency to this project, add a line in `package.json` for it.

* [Express](http://expressjs.com/) - Node.js web Framework.
* [connect-assets](https://github.com/TrevorBurnham/connect-assets) - Auto compiles coffee and stylus
* [CoffeeScript](http://coffeescript.org/) - Adds syntactic sugar and compiles to JS.
* [Jade](http://jade-lang.com/) - Node template engine.
* [Stylus](http://learnboost.github.com/stylus/) - Expressive, dynamic, robust CSS.

### Cake Commands

The server is running on `localhost:9001`

* `cake server` - Start the server

### Troubleshooting

* Can't run `cake` or any other module installed with `npm`?
 * Add PROJECT_ROOT/node_modules/.bin to your system PATH.
 * `export PATH="~/path/to/beez/node_modules/.bin:$PATH"`
<br/>
<br/>
